#ifndef KEEBLER_CARDKB
#define KEEBLER_CARDKB

// Arduino
#include "Wire.h"

// Local

#include "KeyMatrix.hpp"
#include "KeyBuffer.hpp"
#include "GenericSink.hpp"
#include "InputEvents.hpp"
#include "StatusLight.hpp"

class CardKB
: public GenericSink<InputEvents::Event>
{
public:

  typedef enum Mode
  {
      NORMAL      = 0,
      SHIFT       = 2,
      SHIFT_LOCK  = 3,
      SYMBOL      = 4,
      SYM_LOCK    = 5,
      FUNCTION    = 6,
      FN_LOCK     = 7,
  } Mode;

  explicit CardKB(
      GenericSink<unsigned char> *,
      StatusLight *
  );

  void poll();

  bool accept(InputEvents::Event) override;

private:
  GenericSink<unsigned char> * const characterSink;
  //Adafruit_NeoPixel * const statusLight;
  StatusLight * const statusLight;

  InputEvents inputEvents;
  KeyMatrix matrix;

  Mode mode;
  KeyMatrix::Button activeKey;

  size_t modeEnterTime;
  size_t flashUntil;

  static unsigned char const KEYMAP[192];

private:

  static bool isSticky(CardKB::Mode);

public:
  static unsigned char mapKey(CardKB::Mode, KeyMatrix::Button);

// KEEBLER_CARDKB
};

#endif