#ifndef KEEBLER_CARD_MODIFIER
#define KEEBLER_CARD_MODIFIER

typedef enum class CardModifier
{
    NONE  = 0,
    SHIFT = 1,
    SYM   = 2,
    FN    = 3,
} CardModifier;

#endif
