#ifndef KEEBLER_GENERICSINK
#define KEEBLER_GENERICSINK

template<typename T>
class GenericSink
{
public:
  virtual bool accept(T item)
  { return false; }
};

//KEEBLER_GENERICSINK
#endif
