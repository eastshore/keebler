#ifndef KEEBLER_INPUTEVENTS_H
#define KEEBLER_INPUTEVENTS_H

#include "KeyMatrix.hpp"
#include "GenericSink.hpp"

class InputEvents
    : public GenericSink<KeyMatrix::Status>
{
public:
  typedef union KeyId
  {
    KeyMatrix::Modifier modifier;
    KeyMatrix::Button button;
  } KeyId;

  typedef enum class Type
  {
    MODIFIER_PRESS,
    MODIFIER_UP,
    KEY_PRESS,
    KEY_UP,
  } Type;

  typedef unsigned long Timestamp;

  typedef struct Event
  {
    Type type;
    KeyId id;
    Timestamp timestamp;
  } Event;

public:
  explicit InputEvents(GenericSink<Event> *);

  bool accept(KeyMatrix::Status) override;

private:

  GenericSink<Event> *sink;

  typedef unsigned long Time;

  KeyMatrix::Button pendingKey;
  KeyMatrix::Button currentKey;

  Time keyStartTime;

  KeyMatrix::Modifier pendingMod;
  KeyMatrix::Modifier currentMod;

  Time modStartTime;

  Event currentEvent;
};


#endif //KEEBLER_INPUTEVENTS_H
