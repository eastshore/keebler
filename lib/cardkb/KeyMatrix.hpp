#ifndef CARDKB_KEYMATRIX
#define CARDKB_KEYMATRIX

#include <stdint.h>

#include "GenericSink.hpp"

class KeyMatrix
{
public:
  // All the buttons on the matrix
  typedef enum class Button
  {
    ESCAPE = 0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO, DEL,
    TAB,              Q, W, E, R, T, Y, U, I, O, P,
    NO_KEY,
    LEFT, UP,         A, S, D, F, G, H, J, K, L, ENTER,
    DOWN, RIGHT,      Z, X, C, V, B, N, M, COMMA, PERIOD, SPACE,
  } Button;

  typedef enum class Modifier
  {
    NONE  = 0,
    SHIFT = 1,
    SYM   = 2,
    FN    = 3,
  } Modifier;

  typedef unsigned long Timestamp;

  typedef struct Status
  {
    Modifier  mod;
    Button    key;
    Timestamp timestamp;
  } Status;

public:

  explicit KeyMatrix(GenericSink<Status> *);

  Status poll();

private:

  GenericSink<Status> * const statusSink;
  bool isSetup;
  Status currentStatus;

private:

  void setup();

  bool pollBottomRow();

  bool pollSecondRow();

  bool pollThirdRow();

  bool pollTopRow();
};

// CARDKB_KEYMATRIX
#endif