#ifndef KEEBLER_KEYROW_H
#define KEEBLER_KEYROW_H

#include <stdint.h>
#include "KeyBank.hpp"
#include "CardButton.hpp"

class KeyRow
{

public:
    KeyRow(int, KeyBank *);

    CardButton pollOne() const;

private:

    int const bankCount;
    KeyBank * const banks;
};


#endif //KEEBLER_KEYROW_H
