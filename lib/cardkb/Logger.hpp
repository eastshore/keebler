#ifndef KEEBLER_LOGGER_HPP
#define KEEBLER_LOGGER_HPP

#ifdef DEBUG_SERIAL
#include <stdio.h>
#include "HardwareSerial.h"
#define LOG_OUTPUT(DATA)  Serial.print(DATA)
#else
#define LOG_OUTPUT(DATA)  (DATA)
#endif

#define BITS_PARAMS(BYTE) \
    (((BYTE) & 0x80) ? '1' : '0'), \
    (((BYTE) & 0x40) ? '1' : '0'), \
    (((BYTE) & 0x20) ? '1' : '0'), \
    (((BYTE) & 0x10) ? '1' : '0'), \
    (((BYTE) & 0x8)  ? '1' : '0'), \
    (((BYTE) & 0x4)  ? '1' : '0'), \
    (((BYTE) & 0x2)  ? '1' : '0'), \
    (((BYTE) & 0x1)  ? '1' : '0')

void LOG();

void LOG_LINE();

template <typename T>
void LOG(T arg)
{
    LOG_OUTPUT(arg);
    LOG();
}

template <typename T>
void LOG_LINE(T arg)
{
    LOG_OUTPUT(arg);
    LOG_LINE();
}

template <typename Car, typename... Cdr>
void LOG(Car arg, Cdr... args)
{
    LOG(arg);
    LOG(args...);
    LOG();
}

template <typename Car, typename... Cdr>
void LOG_LINE(Car arg, Cdr... args)
{
    LOG(arg);
    LOG(args...);
    LOG_LINE();
}

// KEEBLER_LOGGER_HPP
#endif