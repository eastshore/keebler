#include "StatusLight.hpp"

#include "Arduino.h"

#define LED_PINB_PIN 5
#define BLINK_ON_TIME 900

#define COLOR_FLASH ws2811::rgb(4, 2, 2)
#define COLOR_OFF ws2811::rgb(0, 0, 0)
#define COLOR_SHIFT ws2811::rgb(1, 4, 1)
#define COLOR_SYM ws2811::rgb(1, 1, 4)
#define COLOR_FN ws2811::rgb(4, 1, 1)

StatusLight::StatusLight() :
    statusLed({COLOR_OFF})
{}

void StatusLight::requestState(StatusLight::Status const state)
{
  auto pulseOff = ((millis() / 800) % 2) == 0;

  // while flashing, override states
  if (blinkUntil > millis())
  {
    statusLed[0] = COLOR_FLASH;
    return;
  }

  switch (state)
  {
    case Status::SHIFT:
      if (pulseOff)
      {
        statusLed[0] = COLOR_OFF;
        break;
      }
    case Status::SHIFT_LOCK:
      statusLed[0] = COLOR_SHIFT;
      break;
    case Status::FN:
      if (pulseOff)
      {
        statusLed[0] = COLOR_OFF;
        break;
      }
    case Status::FN_LOCK:
      statusLed[0] = COLOR_FN;
      break;
    case Status::SYM:
      if (pulseOff)
      {
        statusLed[0] = COLOR_OFF;
        break;
      }
    case Status::SYM_LOCK:
      statusLed[0] = COLOR_SYM;
      break;
    case Status::OFF:
    default:
      statusLed[0] = COLOR_OFF;
  }
}

void StatusLight::requestFlash()
{
  blinkUntil = millis() + BLINK_ON_TIME;
}

void StatusLight::flush()
{
  send(statusLed, 1, LED_PINB_PIN);
}
