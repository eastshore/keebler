#ifndef KEEBLER_STATUSLIGHT_HPP
#define KEEBLER_STATUSLIGHT_HPP

#include "ws2811/ws2811.h"

class StatusLight
{
public:
  enum class Status
  {
    OFF,
    SHIFT,
    SHIFT_LOCK,
    SYM,
    SYM_LOCK,
    FN,
    FN_LOCK,
  };

public:
  explicit StatusLight();

  void requestState(Status);

  void requestFlash();

  void flush();

private:

  ws2811::rgb statusLed[1];

  unsigned long blinkUntil;

  /*
  static ws2811::rgb const COLOR_FLASH;
  static ws2811::rgb const COLOR_OFF;
  static ws2811::rgb const COLOR_SHIFT;
  static ws2811::rgb const COLOR_SYM;
  static ws2811::rgb const COLOR_FN;
   */
};


#endif //KEEBLER_STATUSLIGHT_HPP
