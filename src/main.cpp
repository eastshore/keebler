#include <Arduino.h>

#include <stdio.h>

#include <Wire.h>
#include "CardKB.hpp"
#include "KeyBuffer.hpp"
#include "Logger.hpp"
#include "StatusLight.hpp"

#define BUFFER_LEN 16

// Do some templating trickery to hide the array underlying the buffer
template<typename T> using alias = T;

KeyBuffer keyBuffer(BUFFER_LEN, alias<unsigned char[BUFFER_LEN]>{});
StatusLight statusLed;
CardKB keyboard(&keyBuffer, &statusLed);

void flush_buffer()
{
  if (keyBuffer.isNotEmpty())
  {
    auto const keyStroke = keyBuffer.take();
    Wire.write(keyStroke);
  }
}

void setup()
{
#ifdef DEBUG_SERIAL
  Serial.begin(9600);
  LOG_LINE("\r\nsup b");
#endif
  Wire.begin(0x5f);
  Wire.onRequest(flush_buffer);

  statusLed.requestFlash();
  statusLed.flush();
}

#define TARGET_SAMPLE_PERIOD    5

static unsigned long lastLoop = 0L;

void loop()
{
  /*
  auto const loopEnter = millis();

  if (lastLoop == 0 || (lastLoop + TARGET_SAMPLE_PERIOD) < loopEnter)
  {
    keyboard.poll();
    lastLoop = loopEnter;
  }

#ifdef DEBUG_SERIAL
  if (keyBuffer.isNotEmpty())
  {
    LOG_LINE("Got input: ", static_cast<char>(keyBuffer.take()));
  }
#endif

  statusLed.flush();
   */
  statusLed.requestState(StatusLight::Status::SHIFT);
  statusLed.flush();
}
